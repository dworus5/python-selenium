import pytest

from _2_pytest.my_queue import MyQueue


def empty_queue() -> MyQueue:
    # TODO Use this method as fixture, return initialized queue object
    # TODO Fixture functions are decorated with @pytest.fixture
    pass


def non_empty_queue() -> MyQueue:
    # TODO Use this method as fixture, return initialized queue object
    # TODO Fixture functions are decorated with @pytest.fixture
    pass


def test_queue_is_empty_when_initialized():
    # TODO Use empty_queue fixture and test the queue
    pytest.fail("Not implemented! Remove this line.")


def test_element_is_added_on_top_of_the_queue():
    # TODO Use non_empty_queue fixture, add element and verify the element was removed as first
    pytest.fail("Not implemented! Remove this line.")
