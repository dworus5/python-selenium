from _2_pytest.samples.words import reverse


def test_my_test_1():
    # act
    expected = reverse("Pytest")
    # assert
    assert expected == "tsetyP"


def test_my_test_2():
    # act
    expected = reverse("pytest")
    # assert
    assert expected == "tsetyp"
