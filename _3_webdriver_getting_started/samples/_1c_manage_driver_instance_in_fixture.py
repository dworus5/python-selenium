import pytest
from selenium.webdriver import Firefox
from selenium.webdriver import Remote as WebDriver
from selenium.webdriver.firefox.service import Service
from webdriver_manager.firefox import GeckoDriverManager

from common import config


@pytest.fixture
def driver() -> WebDriver:
    driver = Firefox(service=Service(GeckoDriverManager().install()))
    driver.get(config.WEB_SAMPLES_URL + "/sample1.html")
    yield driver
    driver.close()


def test_1(driver: WebDriver):
    assert "Sample 1" == driver.title


def test_2(driver: WebDriver):  # Assign type
    assert "Sample 1" == driver.title
