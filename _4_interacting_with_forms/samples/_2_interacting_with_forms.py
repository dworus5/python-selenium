import pytest
from time import sleep

from common import config, helpers
from selenium.webdriver import Remote as WebDriver
from selenium.webdriver.common.by import By


@pytest.fixture(scope="module")
def driver_instance() -> WebDriver:
    driver = helpers.new_driver()
    yield driver
    driver.close()


@pytest.fixture
def driver(driver_instance) -> WebDriver:
    driver_instance.get(config.WEB_SAMPLES_URL + "/sample3.html")
    yield driver_instance
    driver_instance.delete_all_cookies()


def test_1(driver: WebDriver):
    field1 = driver.find_element(By.ID, "field1")
    field1.clear()
    field1.send_keys("Value 1")

    field2 = driver.find_element(By.ID, "field2")
    field2.clear()
    field2.send_keys("Value 2")

    field3 = driver.find_element(By.ID, "field3")
    field3.clear()
    field3.send_keys("Value 3")

    checkboxes = driver.find_elements(By.CLASS_NAME, "md-checkbox-container")
    checkboxes[0].click()
    checkboxes[1].click()

    radios = driver.find_elements(By.CLASS_NAME, "md-radio-container")
    radios[1].click()

    switch1 = driver.find_element(By.CLASS_NAME, "md-switch-container")
    switch1.click()

    submit = driver.find_element(By.ID, "submit")
    submit.click()

    # Sleep for a moment to show the dialog
    sleep(1)
