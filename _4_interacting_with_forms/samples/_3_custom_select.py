from time import sleep

import pytest
from selenium.webdriver import Remote as WebDriver
from selenium.webdriver.common.by import By

from common import config, helpers


@pytest.fixture(scope="module")
def driver_instance() -> WebDriver:
    driver = helpers.new_driver()
    yield driver
    driver.close()


@pytest.fixture
def driver(driver_instance) -> WebDriver:
    driver_instance.get(config.WEB_SAMPLES_URL + "/sample4.html")
    yield driver_instance
    driver_instance.delete_all_cookies()


def test_1(driver: WebDriver):
    # Get all selects list

    selects = driver.find_elements(By.CSS_SELECTOR, "div.md-select")

    # Handle single selection

    select_single = selects[0]
    select_single.click()

    menu = driver.find_element(By.CLASS_NAME, "md-menu-content-container")
    menu_items = menu.find_elements(By.TAG_NAME, "li")
    menu_items[1].click()

    sleep(1)

    # Multiple selection

    select_multi = selects[1]
    select_multi.click()

    menu_items = driver.find_elements(By.CLASS_NAME, "md-list-item")
    menu_items[0].click()
    menu_items[1].click()

    # Sleep for a moment to show the dialog
    sleep(1)
