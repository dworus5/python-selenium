import pytest
from selenium.webdriver import ActionChains
from selenium.webdriver import Remote as WebDriver
from selenium.webdriver.common.by import By

from common import helpers


@pytest.fixture(scope="module")
def driver_instance() -> WebDriver:
    driver = helpers.new_driver()
    yield driver
    driver.close()


@pytest.fixture
def driver(driver_instance) -> WebDriver:
    driver_instance.get('https://jqueryui.com/draggable/')
    driver_instance.switch_to.frame(0)

    yield driver_instance
    driver_instance.delete_all_cookies()


def test_drag_and_drop(driver: WebDriver):
    draggable = driver.find_element(By.ID, "draggable")
    actions = ActionChains(driver)
    actions \
        .click_and_hold(draggable) \
        .pause(2) \
        .move_by_offset(100, 100) \
        .pause(2) \
        .move_by_offset(100, 100) \
        .pause(2) \
        .move_by_offset(100, -100) \
        .pause(2) \
        .move_by_offset(100, -100) \
        .release() \
        .perform()
