import pytest
from selenium.webdriver import ActionChains
from selenium.webdriver import Remote as WebDriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait


@pytest.fixture
def driver(login) -> WebDriver:
    # Returns already logged in user
    return login


def test_get_default_contacts_names(driver: WebDriver):
    # TODO Implement.
    pytest.fail("Not implemented! Remove this line.")


def test_search_for_single_contact_by_name(driver: WebDriver):
    # TODO Implement.
    pytest.fail("Not implemented! Remove this line.")


def test_filter_by_one_label(driver: WebDriver):
    # TODO Implement.
    pytest.fail("Not implemented! Remove this line.")


def test_delete_selected_contacts(driver: WebDriver):
    # TODO Implement.
    pytest.fail("Not implemented! Remove this line.")


def test_show_favorites(driver: WebDriver):
    # TODO Implement. Use WebDriverWait to wait for the snackbar
    pytest.fail("Not implemented! Remove this line.")


#
# Optional
#

def test_search_for_two_contacts_by_email(driver: WebDriver):
    # TODO Implement.
    pytest.fail("Not implemented! Remove this line.")


def test_search_for_no_results(driver: WebDriver):
    # TODO Implement.
    pytest.fail("Not implemented! Remove this line.")


def test_filter_by_two_labels(driver: WebDriver):
    # TODO Implement.
    pytest.fail("Not implemented! Remove this line.")


def test_filter_by_non_existing_label(driver: WebDriver):
    # TODO Implement.
    pytest.fail("Not implemented! Remove this line.")
